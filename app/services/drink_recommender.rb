class DrinkRecommender
    def initialize(opt = {})
        self.ibu = opt[:ibu]
        self.temperature = opt[:temperature]
        self.description = opt[:description]
    end

    def call
        (by_ibu | by_temperature | by_description).sort_by { |drink | drink.rating_avg }.reverse
    end

    private
    
    def by_ibu
        Drink.where(ibu: ibu_range)
    end

    def ibu_range
        ibu ? (ibu - 2)..(ibu + 2) : []
    end

    def temperature_range
        temperature ? ( Drink.temperatures[temperature] - 1)..(Drink.temperatures[temperature] + 1 ) : []
    end

    def by_temperature
        temperature ? Drink.where(temperature: temperature_range) : []
    end

    def by_description
        return [] unless description
        stopwords = %w{the a by on for of are with just but and to the my in I has some}
        usefull_words = description.split(/\.|\?|!/).select { |word| !stopwords.include?(word) }
        by_keywords(usefull_words)
    end

    def by_keywords(prases)
        prases.map do |w|
            Drink.where('description ILIKE ?  OR name ILIKE ?', "%#{w}%", "%#{w}%")
        end.uniq(&:ids).flatten
    end


    attr_accessor :ibu, :temperature, :description
end