class DrinksController < ApplicationController
  def index
    @drinks = Drink.all.order(:created_at)
  end

  def search
    @drinks = Drink.where('name ILIKE ? OR description ILIKE ?', "%#{params[:search]}%", "%#{params[:search]}%")
    render json: @drinks
  end

  def recommend
    @drinks = DrinkRecommender.new(preferences).call
    render json: @drinks
  end

  private

  def preferences
    prefs = params.permit(:ibu, :temperature, :description).reject{|_, value| value.blank?}
    prefs[:ibu] = prefs[:ibu].to_i if prefs[:ibu]
    prefs
  end
end
