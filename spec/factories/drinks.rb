FactoryBot.define do
  factory :drink do
    name 'Drink A'
    image_url 'https://example.com/image.jpg'
    description 'A drink that has a lot of water'
    rating_avg 1
    alcohol_level 1
    ibu 1
    temperature :hot
    
    trait :pilsen do
      name "Pilsen"
      ibu 7
      description 'Pilsner is a type of pale lager. It takes its name from the Bohemian city of Pilsen, where it was first produced in 1842. The world’s first blond lager, the original Pilsner Urquell, is still produced there today.'
      rating_avg 4
      temperature :extra_cold
    end

    trait :caipirinha do
      name "Caipirinha"
      ibu 10
      description 'Caipirinha is Brazil\'s national cocktail, made with cachaça, sugar and lime. Cachaça, also known as caninha, or any one of a multitude of traditional names, is Brazil\'s most common distilled alcoholic beverage.'
      rating_avg 4
      temperature :cold
    end
  end
end
