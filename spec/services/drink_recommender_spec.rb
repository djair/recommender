require 'rails_helper'

RSpec.describe DrinkRecommender do
 let(:subject) { described_class.new(params) }
 
 before(:each) do
    @pilsen = create(:drink, :pilsen)
    @caipirinha = create(:drink, :caipirinha)
 end   

  describe '#call' do
    context 'ibu' do
      let(:params) { { ibu: 5 } }
      it 'shoud return all ibu 2 - 6' do
        expect(subject.call).to eq([@pilsen])    
      end
    end

    context 'temperature' do
      let(:params) { { temperature: :cold } }
      it 'shoud return all :cold - extra_cold' do
        expect(subject.call).to eq([@pilsen, @caipirinha])    
      end
    end

    context 'text' do
      let(:params) { { description: 'pale lager. Bohemian city' } }
      it 'shoud return all matching prases' do
        expect(subject.call).to eq([@pilsen])    
      end
    end
  end
end