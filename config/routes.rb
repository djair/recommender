Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'drinks#index'

  resources :drinks, only: %i[index] do
    get :search, on: :collection
    get :recommend, on: :collection
  end
end
